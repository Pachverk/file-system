<?php


namespace Pachverk;


class Archive
{
    /**
     * UnZip archive file
     * @param string $archivePath
     * @param string $putPath
     * @throws \Exception
     */
    static function unZip(string $archivePath, $putPath = '.')
    {
        $archive = new \SplFileInfo($archivePath);

        if (!class_exists(\ZipArchive::class)) {
            throw new \Exception("Can't find class (" . \ZipArchive::class . ")" . PHP_EOL);
        }

        if ($archive->getExtension() !== 'zip') {
            throw new \Exception("Can't find archive <{$archive->getRealPath()}>" . PHP_EOL);
        }

        $zip = new \ZipArchive;
        if ($zip->open($archive->getRealPath()) === true) {
            $zip->extractTo($putPath);
            $zip->close();
        } else {
            throw new \Exception("Can't unZip archive");
        }
    }
}